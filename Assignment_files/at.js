utils.define('at.query').extend('jqtag').as(function(test){
	
	_require_(":jqgeeks/jquery_ui","jq2much",":jqtags/jq-date",":bootstrap-select");
	var JSONUTIL = _module_("jqutils.jsonutil");
	//utils.require('resources/bootstrap-select/dist/js/bootstrap-select.js');
	//utils.require('resources/bootstrap-datepicker/dist/js/bootstrap-datepicker.js');
	
	var $ = jQuery;
	var $panels,$pointer;
	var $this;
	
	var config, entityHTML = "",entityValue,entityOptions,entityType,relationValue;
	
	test.register({
	    tagName: "at-query",
	    events: {
	        "change .entity" : "onEntityChange",
	        "change .rel2Input" : "onRelationChange",
	       // "mouseover .dateInput " : "statDatePicker"
	    },
	    accessors: {
	        count: {
	            type: "int",
	            default : -1,
	            onChange : "onCountChange"
	        }
	    },	
	    attachedCallback: function () {
	    	var self = this;
	    	
	    	self.$.innerHTML = '<div class="divEntity"><span class="holder">#</span>'+
	    	entityHTML 
	    	+'</div><div class="divRelation" >'
	    	+ '<select class="rel2Input selectpicker"></select>' +
	    	'</div><div class="divValue"></div>';
	    	$('.selectpicker',self.$).selectpicker({
	    		dropupAuto : false
	    	});
	    	this.onEntityChange(null,$('.entity',this.$)[0])
	    },
	    onEntityChange : function(e,target){
	    	$this  = $(this.$);
	    	entityValue = target.value;
	    	this.entityOptions = config.conditions.filter(function(option){
				return entityValue === option.name
			})[0];
	    	var $relation = $this.find('.rel2Input').first();
	    	$relation.html(test.getDropDownOptions(
	    			config.types[this.entityOptions.type].options
	    	)).selectpicker("refresh");
	    	this.onRelationChange(null,$relation[0])
	    },
	    onRelationChange : function(e,target){
	    	$this = $(this.$);
	    	var relationValue = target.value;
	    	var typeOptions = config.types[this.entityOptions.type];
	    	if(typeOptions.control === "dropdown"){
	    		$('.divValue',this.$).html('<select class="selectpicker value" '+(typeOptions.multiple ? 'multiple' : '')+'>'+
    				test.getDropDownOptions(
    						this.entityOptions.options
    		    	)	
	    		+'</select>');
		    	$('.selectpicker',this.$).selectpicker({
		    		dropupAuto : false
		    	});
	    	} else if(typeOptions.control === "date"){
	    		var relationVAL = relationValue.toUpperCase().replace(" ", "_");
	    		var inputHTML = "";
	    		if(relationVAL==="IS_BETWEEN"){
	    			inputHTML = '<jq-date class="dateInput startDate"></jq-date><jq-date class="dateInput endDate"></jq-date>';
	    		} else {
	    			inputHTML = '<jq-date class="dateInput startDate"></jq-date>';
	    			//inputHTML = '<div class="input-append date dateInput"><input class="startDate"/><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span></div>';
	    		}
	    		$('.divValue',this.$).html('<jq-repeat><jq-repeat-push>[+]</jq-repeat-push><jq-repeat-panel>' 
	    				+ inputHTML + '</jq-repeat-panel><jq-repeat-default><jq-repeat-remove>X</jq-repeat-remove>' 
	    				+ inputHTML + '</jq-repeat-default></jq-repeat>');
	    	}
	    },
	    statDatePicker : function(e,target){
	    	if(!$(target).hasClass("initd")){
	    		$(target).addClass("initd").datepicker();
	    	}
	    },
	    render : function(e){
	    	var self = this;
	    	$this = $(this.$);
	    	this.$.innerHTML = "";
	    	for(var i=0; i<this.$.count;i++){
	    		this.$.innerHTML = this.$.innerHTML + self.repeatHTML;
	    	}
	    }
	});
	
	test.getDropDownOptions = function(options){
		var optionsHTML = "";
		for(var i in options){
			optionsHTML +=('<option>'+options[i]+'</option>');
		}
		return optionsHTML;
	};
	
	test._ready_ = function(){
		console.error(this.getPath("at.query.meta.json")+"")
		this.getPath("at.query.meta.json").load().done(function(resp){
			config = JSONUTIL.parse(resp)
			entityHTML += '<select class="selectpicker entity">';
			for(var i in config.conditions){
				entityHTML +=('<option>'+config.conditions[i].name+'</option>')
			}
			entityHTML += '</select>';
		});
		
		$('body').on("pushitem",".level1.level", function(e){
			$(".level3.level .item").each(function(i,elem){
				var $elem = $(elem)
				$elem.sortable({
					connectWith : ".level3.level .item",
					helper: "clone", forceHelperSize : true,
					opacity: 0.5,
					remove : function(e,e2){
						$elem.closest(".level3")[0].count--;
					}
				});
			})
		});
		
	};
});