utils.define('jqtags.repeat').extend('jqtag').as(function(test){
	
	utils.require("jq2much");
	
	var $ = jQuery;
	var $panels,$pointer;
	var $this;
	
	test.register({
	    tagName: "jq-repeat",
	    events: {
	        "click jq-repeat-push" : "push",
	        "click jq-repeat-pop" : "pop",
	        "click jq-repeat-remove" : "remove"
	    },
	    accessors: {
	        count: {
	            type: "int",
	            default : -1,
	            onChange : "onCountChange"
	        }
	    },	
	    attachedCallback: function () {
	    	//console.error("attachedCallback")
	    	var self = this;
	    	$this = $(this.$);
	    	var defaultTag = this.findSafe("jq-repeat-default").attr("hidden","hidden").first();
	    	//console.debug("defaultTag",this,defaultTag.length,defaultTag);
	    	if(defaultTag.length == 1){
	    		self.repeatHTML = defaultTag[0].innerHTML;
	    		//defaultTag[0].innerHTML = "";
	    	} else {
	    		//console.debug("NOT FOUCND")
	    		self.repeatHTML = "";
	    		//console.debug($this.find("jq-repeat"))
	    	}
	    	//console.error(self.$.getAttribute("class"),"===",self.repeatHTML)
	    	self.onCountChange();
	    	self.setNumbers();
	    },
	    findSafe : function(selector){
	    	return $this.findSafe(selector,"jq-repeat");
	    },
	    onCountChange : function(){
	    	var self = this;
	    	$this = $(this.$);
	    	$panels = this.findSafe("jq-repeat-panel");
	    	if(!(self.$.count>=0)){
	    		self.$.count = $panels.length;
	    	} else if(self.$.count>$panels.length){
	    		$pointer = $panels[$panels.length-1];
	    		if($pointer === undefined){
	    			$pointer = this.findSafe("jq-repeat-default")[0];
	    			if($pointer === undefined){
	    				$pointer = document.createElement("jq-repeat-default");
	    				this.$.appendChild($pointer);
	    			}
	    		}
	    		for(var i=$panels.length;i<self.$.count; i++){
		    		$pointer = self.pushAfter($pointer, i);
	    		}
	    		self.makePointer();
	    	} else if(self.$.count<$panels.length) {
	    		for(var i= $panels.length-1;i>=self.$.count; i--){
	    			$panels[i].remove();
	    		}
	    	}
	    },
	    makePointer : function(){
	    	$this = $(this.$);
	    	this.findSafe("jq-repeat-remove,jq-repeat-push,jq-repeat-pop").attr("pointer",true);
	    },
	    setNumbers : function(){
	    	$this = $(this.$);
	    	$panels = this.findSafe("jq-repeat-panel");
	    	for(var i=0; i<$panels.length; i++){
	    		$panels[i].setAttribute("jq-repeat-index",i);
	    	}
	    },
	    pushAfter : function($pointer,index){
	    	var self = this;
	    	$newPointer = $("<jq-repeat-panel jq-repeat-index="+index+">"+self.repeatHTML+"</jq-repeat-panel>");
	    	$($pointer).after($newPointer);
	    	return $newPointer;
	    },
	    push : function(e){
	    	this.$.count++;
	    	this.onCountChange();
	    	this.trigger("pushitem",{count : this.$.count});
	    	return window.preventPropagation(e);
	    },
	    pop : function(e){
	    	this.$.count--;
	    	this.onCountChange();
	    	this.trigger("popitem",{count : this.$.count});
	    	return window.preventPropagation(e);
	    },
	    remove : function(e,target){
	    	target.closest("jq-repeat-panel").remove();
	    	this.$.count--;
	    	this.setNumbers();
	    	this.trigger("removeitem",{count : this.$.count});
	    	return window.preventPropagation(e);
	    },
	    render : function(e){
	    	var self = this;
	    	$this = $(this.$);
	    	this.$.innerHTML = "";
	    	for(var i=0; i<this.$.count;i++){
	    		this.$.innerHTML = this.$.innerHTML + self.repeatHTML;
	    	}
	    	
	    }
	});
	
});