utils.config.set({
	debug : true,
	contextPath : CONTEXT_PATH,
	bundle_list : CONTEXT_PATH+"resources/bundle.json",
	combineJS : !!RX_JS_MERGE,
	version : RELOAD_VERSION,
	mergeJS : function(params,encoded){
		return CONTEXT_PATH + "combinejs/"+encoded+".js";
	}
	/*moduleConfig : {
		'jqutils.cache.files' : {
			cache_script : !!(RX_UI_CACHE)
		}
	}*/
});
