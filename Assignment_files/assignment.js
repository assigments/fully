utils.define("assignment.app").extend("spamjs.module").as(function(app,_app_){
	
	var router = utils.module("jqrouter");
	var jsonutil = utils.module("jqutils.jsonutil");
	utils.require("jqtags.switch","jqtags.slider", "at.query");
	
	_app_._init_ = function(){
		var data = {};
		var self = this;
		
		router.on("/assignment",function(){
			var html_index = "boxtree";
			var page = "index";
			self.style(html_index + "/style.scss");
			app.getPath(html_index+"/data.json").load().done(function(resp){
				data = jsonutil.parse(resp)
			}).always(function(){
				//To render other pages
				self.load({
					src : html_index + "/"+page+".html",
					data : data
				}).done(function(){
					app.pageReady();
				});
			});
		});
		
	};
	app._ready_ = function(){
		app.instance().init();
	};
	
	app.pageReady = function(){
		console.log("page ready",$("#sampeltab"));
	}
});