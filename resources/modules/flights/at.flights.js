_define_("at.flights", "spamjs.module", function(flights, _flights_) {

	var jq = _module_("jQuery");

	_flights_._init_ = function() {
		var self = this;

		this.model({
			name : "Lalit"
		});
		
		this.load({
			src : "at.flights.html",
			data : "at.flights.json"
		}).done(function() {
			console.log("view is loaded");
			self.doBindings();
		});
	};

	_flights_.doBindings = function() {
		var self = this;

		self.$$.find('.resultdetailbox, .resultdetailboxInd').hide();
		self.$$.find('.resultboxdiv, .resultboxdivInd, .downArrow').click(
				function() {
					var self2 = jq(this).parent();
					self2.children(".resultdetailbox, .resultdetailboxInd")
							.slideToggle(300);
				});

		self.$$.find('.filterHd').click(function() {
			if (jq(this).next().is(':hidden') != true) {
				jq(this).removeClass('active');
				jq(this).next().slideUp("normal");
			} else {
				self.$$.find('.filterHd').removeClass('active');
				self.$$.find('.filterContain').slideUp('normal');
				if (jq(this).next().is(':hidden') == true) {
					jq(this).addClass('active');
					jq(this).next().slideDown('normal');
				}
			}
		});

		self.$$.find('.expand').click(function(event) {
			self.$$.find('.filterHd').next().slideDown('normal');
			{
				self.$$.find('.filterHd').addClass('active');
			}
		});

		self.$$.find('.collapse').click(function(event) {
			self.$$.find('.filterHd').next().slideUp('normal');
			{
				self.$$.find('.filterHd').removeClass('active');
			}
		});

	//TODO:-Note - Not needed any more as converted into x-tag,
	//Now need to listen to on chnage event of those HTML tags
//		self.$$.find(".fareRange").slider(
//				{
//					range : true,
//					min : 0,
//					max : 50000,
//					values : [ 0, 50000 ],
//					slide : function(event, ui) {
//						self.$$.find(".amount").val(
//								"₹" + ui.values[0] + " - ₹" + ui.values[1]);
//					}
//				});
//		self.$$.find(".amount").val(
//				"₹" + self.$$.find(".fareRange").slider("values", 0) + " - ₹"
//						+ self.$$.find(".fareRange").slider("values", 1));
		
		self.$$.find("#pricerange").on("change", function(e){
			var value = jq(this).val().split(",");
			self.$$.find(".amount").val(
			"₹" + value[0] + " - ₹"
					+ value[1]);
		});

	}

	_flights_._remove_ = function() {

	};

})