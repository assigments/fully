_define_("aertrip.cabsreceipts",function(cabsreceipts) {

  cabsreceipts.initialize = function(){

//-----------------------------------------------------------//
   var tabs = $('.tabs-titles li'); //grab tabs
	var contents = $('.tabs-contents li'); //grab contents

	tabs.bind('click',function(){
	  contents.hide(); //hide all contents
	  tabs.removeClass('current'); //remove 'current' classes
	  $(contents[$(this).index()]).show(); //show tab content that matches tab title index
	  $(this).addClass('current'); //add current class on clicked tab title
	});
//-----------------------------------------------------------//
};

});