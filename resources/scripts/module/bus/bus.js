_define_("aertrip.bus",function(bus) {

  bus.initialize = function(){


//-----------------------------------------------------------//
	$('.resultdetailbox').hide();	
	$('.resultboxdiv, .downArrow').click(function(){		
		var self = $(this).parent();
		self.children(".resultdetailbox").slideToggle(300);	
	});
//-----------------------------------------------------------//


//-----------------------------------------------------------//
	$('.filterHd').click(function() {
 
	  if($(this).next().is(':hidden') != true) {
	     $(this).removeClass('active'); 
	     $(this).next().slideUp("normal");
	  } else {
	    $('.filterHd').removeClass('active');  
	     $('.filterContain').slideUp('normal');
	    if($(this).next().is(':hidden') == true) {
	    $(this).addClass('active');
	    $(this).next().slideDown('normal');
	     }   
	  }
	   });
	 
	  // $('.filterContain').hide();
	 
	  $('.expand').click(function(event)
	    {$('.filterHd').next().slideDown('normal');
	        {$('.filterHd').addClass('active');}
	    }
	  );
	 
	  $('.collapse').click(function(event)
	    {$('.filterHd').next().slideUp('normal');
	        {$('.filterHd').removeClass('active');}
	    }
	  );
//-----------------------------------------------------------//


//-----------------------------------------------------------//
    $( ".fareRange" ).slider({
      range: true,
      min: 0,
      max: 50000,
      values: [ 0, 50000 ],
      slide: function( event, ui ) {
        $( ".amount" ).val( "₹" + ui.values[ 0 ] + " - ₹" + ui.values[ 1 ] );
      }
    });
    $( ".amount" ).val( "₹" + $( ".fareRange" ).slider( "values", 0 ) +
      " - ₹" + $( ".fareRange" ).slider( "values", 1 ) 
      );
//-----------------------------------------------------------//
  };

});