$(document).ready(function() {
	
	$( ".flightOrigin" ).autosearch({
		source: [ "c++", "java", "php", "coldfusion coldfusion coldfusion", "javascript", "asp", "ruby" ],
	});
	
	$('.flightPessengerCount').flightCount({
		label : ['Adults', 'Children', 'Infants'],
		maxNum : 4
	})
});