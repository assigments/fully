/*
* jQuery add button 1.0.0, jQuery plugin
*
*/

(function($) {
	$.fn.flightCount = function(options) {
		var defaults = {
			
		};
		
		var settings = $.extend({}, defaults, options)
			$selector = $(this).selector;

		return this.each(function() {
			var $this = $(this);
			var	$label = $(settings.label);
			
			var i;
			var html = "<ul class='ui-flightCountBox'>";
			for(i = 0; i < $label.length; i++){
				html =  html + "<li>";
				html = html + "<span class='label'>"+ $label[i] +"</span>";
				html = html + "<span class='plus'>+</span>";
				html = html + "<span class='input'><input type='text' class='"+ $label[i]+"' value='0'/></span>";
				html = html + "<span class='minus'>-</span>";
				html = html + "</li>";
			}
			html = html + "</ul>";
			$this.append(html)
			$(document).click(function(){
				$('.ui-flightCountBox').css('display','none').removeClass('animated flipInX');
			});
			inputVal = function(a,b,c){
				$(this).child('input').val('Adults'+ a + ', Children'+ b +', Infants'+ c);
			}
			$($this).on('click', function(e){
				e.preventDefault();
				$('.ui-flightCountBox', $this).css('display','block').addClass('animated flipInX');
				return false;
			});
			$($this).on('click', '.plus, .minus', function(e){
				e.preventDefault();
				var _this = $(this).parent('li');
				var val = $('.input input',_this).val();
				if($(this).hasClass('plus')){
					val++;
					$('.input input' ,_this).val(val);
				}else if($(this).hasClass('minus')){
					val--;
					if(val < 0) val = 0;
					$('.input input', _this).val(val);
				}
				return false;
			});
			$($this).on('keyup keydown', '.input input', function(e){
				var _this = $(this).parent('li');
				var val = $(this).val();
				if(e.keyCode == 38){
					val++;
					$(this).val(val);
				}else if(e.keyCode == 40){
					val--;
					if(val < 0) val = 0;
					$(this).val(val);
				}
			});
			
		});
	};
})(jQuery);