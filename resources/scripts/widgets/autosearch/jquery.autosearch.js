/*
* jQuery add button 1.0.0, jQuery plugin
*
*/

(function($) {
	$.fn.autosearch = function(options) {
		var defaults = {
			
		};
		
		var settings = $.extend({}, defaults, options)
			$selector = $(this).selector;

		return this.each(function() {
			var $this = $(this);
			var input = $('input', $this),
				source = settings.source;
			
			$(input).autocomplete({
				source: source,
				appendTo : $this,
				open : function(){
					$('.ui-menu', $this).addClass('animated');
					$('.ui-menu', $this).removeClass('flipOutX').addClass('flipInX');
				},
				close : function(){
					$('.ui-menu', $this).removeClass('flipInX').addClass('flipOutX');
				}
			});
			
			
		});
	};
})(jQuery);