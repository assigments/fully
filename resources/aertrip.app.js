console.log("hi")
_define_("aertrip.app","spamjs.module",function(app,_app_){
	/*
	 * Import packages to be used 
	 */
	_require_(":jqgeeks/jqrouter");
	_require_("jqtags.switch",":jqtags/jq-slider/jquery","jqtags.date",":aertrip/services",":aertrip/flights");
	_require_(":aertrip/common/header");
	
	var ROUTER = _module_("jqrouter");
	var jsonutil = _module_("jqutils.jsonutil");
	
	var HEADER = _module_("at.header");
	var FLIGHTS = _module_("at.flights");
	
	
	_app_._init_ = function(){
		var data = {};
		var self = this;
		self.router = ROUTER.instance();
		/**
		 *  Load Header Module
		 */
		
		self.add(HEADER.instance());
		
		self.router.on("/mockup/{html_index}/*",function(html_index,_page){
			console.error("----",html_index,_page)
			var page = _page || html_index;
			self.load({
				src : "html/"+html_index+".html"
			}).done(function(){
				_require_(CONTEXT_PATH+"resources/scripts/module/"+page+"/"+html_index+".js");
				var MODULE = _module_("aertrip."+html_index);
				if(MODULE){
					MODULE.initialize();
				}
			});
		}).on("/flights/search",function(){
			
			self.add(FLIGHTS.instance({
				id : "main_content"
			}));
			
		}).on("/bus/search",function(){
			
			self.add(FLIGHTS.instance({
				id : "main_content"
			}));
			
		}).otherwise("/flights/search");
		
	};
	
	_app_._remove_ = function(){
		this.router.off();
	};
	
	app._ready_ = function(){
		app.instance().init();
	};
	

});