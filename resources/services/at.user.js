_define_("at.user",function(user,_user_){
	
	var jq = _module_("jQuery");
	
	user.isValid = function(){
		return jq.when({ loggedin : true });
	};
	
	user.getUserInfo = function(){
		return jq.when({ 
			loggedin : true,
			name : "Travis"
		});
	};
	
});