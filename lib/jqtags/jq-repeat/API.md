###Tags

|Tag | Description |
|---|---|
| jq-repeat |  main wrapper for repeated content and related action |
| jq-repeat-push | adds new jq-repeat-panel cloned from jq-repeat-default|
| jq-repeat-pop |  removes the last jq-repeat-panel |
| jq-repeat-panel |  repeated content is wrapped in this tag|
| jq-repeat-default |  content od this tag will be repeated |
| jq-repeat-remove |  clicking on it remove the cuurent containing jq-repeat-panel |


###Attributes

|Tag| Attribute | Value | Description |
|------|-----|------|---|
| jq-repeat |   count  |  number    |  number of times content should be repeated |


###Events
|Tag| Event Attribute | Event | Description |
|------|-----|-----|------|
| jq-tab |onchange|change|  when tab is selected |

###jQuery Methods
|Tag| Method | Arguments | Returns | Description
|------|-----|-----|------|------|
| jq-repeat |attr| 'count' | number |  number of times content is repeated |
| jq-repeat |attr| 'count',number | |  sets the number of times content is repeated |





