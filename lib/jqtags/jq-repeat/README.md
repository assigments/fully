It simply repeats the content inside

```html
<jq-repeat id="repeat1" count="5" >
	<jq-repeat-push>
		PUSH
	</jq-repeat-push>
	<jq-repeat-pop>
		POP
	</jq-repeat-pop>
	
	<jq-repeat-panel>
		<input type="text" value="23"/>
	</jq-repeat-panel>
	<jq-repeat-panel>
		<input type="text" value="233"/>
	</jq-repeat-panel>
	<jq-repeat-default>
			<input type="text"/>
		<jq-repeat-remove>
			X
		</jq-repeat-remove>
	</jq-repeat-default>
</jq-repeat>
```
Using jQuery you can get/set value

```javascript

$("repeat1").attr("count",7);

```
