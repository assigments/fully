<?php



include_once(RUDRA."/core/controller/AbstractController.php");
require_once LIB_PATH.'/michelf/php-markdown/Michelf/MarkdownExtra.inc.php';

use \Michelf\MarkdownExtra;

class SpamDocController extends AbstractController {

	/**
	 * @RequestMapping(url="markdown", cache=true)
	 */
	public function renderTemplate($temp="index"){
		$filePath = LIB_PATH.get_request_param("mdpath").'.md';
		if(file_exists($filePath)){
			$my_text = file_get_contents($filePath);
			echo  MarkdownExtra::defaultTransform($my_text);			
		}
	}
	
	/**
	 * @RequestMapping(url="modules", type=page, cache=true)
	 */
	public function renderModulsePage($temp="index"){
		return "spamhandler";
	}
	
	/**
	 * @RequestMapping(url="module", type=page, cache=true)
	 */
	public function renderModulePage($temp="index"){
		return  "spamhandler";
	}
}