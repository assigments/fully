utils.define("docspam.app").extend("spamjs.module").as(function(app,_app_){
	
	var router = utils.module("jqrouter");
	var jsonutil = utils.module("jqutils.jsonutil");
	utils.require("jqtags.switch","jqtags.slider");
	var docspam = utils.module("docspam");
	
	_app_._init_ = function(){
		var data = {};
		var self = this;
		self.add(docspam.instance());
	};
	app._ready_ = function(){
		app.instance().init();
	};
	
	app.pageReady = function(){
		console.log("page ready",$("#sampeltab"));
	}
});