utils.define("docspam").extend("spamjs.module").as(function(docspam,_docspam_){
	
	//utils.require(":arturadib/strapdown")
	var Router = utils.module("jqrouter");
	var jsonutil = utils.module("jqutils.jsonutil");
	utils.require("jqtags.tab");
	var server = utils.module("tunnel.server");
	
	_docspam_._init_ = function(){
		var data = {};
		var self = this;
		self.router = Router.instance();
		
		self.router.on("/modules",function(){
			self.load({
				src : "modules.html",
				data : "modules.json"
			})
		}).on("/module/{module}/*",function(moduleNname){
			var Module = utils.module(moduleNname);
			self.load({
				src : "module.html", //server.get(Module.getPath()._dir_.replace(CONTEXT_PATH+"lib",""))
				data : { moduleNname : moduleNname }
			}).done(function(){
				var LIB_PATH = CONTEXT_PATH+"lib";
				self.load({
					selector : "#info",
					src : utils.files.get(CONTEXT_PATH+"/markdown?mdpath="+Module.getPath()._dir_.replace(LIB_PATH,"")+"/README")
				}).done(function(){
					docspam.highLightBlocks(self.$$.find("#info"));
					self.$$.find("#info table").addClass("table table-bordered")
				});
				self.load({
					selector : "#api",
					src : utils.files.get(CONTEXT_PATH+"/markdown?mdpath="+Module.getPath()._dir_.replace(LIB_PATH,"")+"/API")
				}).done(function(){
					docspam.highLightBlocks(self.$$.find("#api"))
					self.$$.find("#api table").addClass("table table-bordered")
				});
				
				utils.require(Module.getPath("demo/"+moduleNname+".demo.js").toString())
				self.$$.find("#demo").html("");
				var testModule = utils.module(moduleNname+".demo");
				if(testModule){
					self.add("#demo",testModule.instance({
						id : "demo"
					}));
				}
				self.load({
					selector : "#install",
					src : utils.files.get(CONTEXT_PATH+"/markdown?mdpath="+Module.getPath()._dir_.replace(LIB_PATH,"")+"/INSTALL")
				}).done(function(){
					docspam.highLightBlocks(self.$$.find("#install"))
					self.$$.find("#api table").addClass("table table-bordered")
				});
				
			});
		}).on("/html/{html_index}/*",function(html_index,_page){
			var page = _page || "index";
			self.style(html_index + "/style.scss");
			docspam.getPath(html_index+"/data.json").load().done(function(resp){
				data = jsonutil.parse(resp)
			
			}).always(function(){
				//To render other pages
				self.load({
					src : html_index + "/"+page+".html",
					data : data
				}).done(function(){
					//docspam.pageReady();
				});
			});
		}).otherwise("/modules");
		
	};
	_docspam_._remove_ = function(){
		this.router.off();
	};
	
	docspam.highLightBlocks = function($block){
		$block.find("code").each(function(i,elem){
			hljs.highlightBlock(elem)
		})
	};
	
});