<?php

/*
 * To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
/**
 * 
 * @Handler(assignment)
 */
class AssignMentHandler extends AbstractHandler {

	public function invokeHandler(Smarty $viewModel,Header $header, DataModel $dataModel,
			AbstractUser $user,$view="empty") {
		$header->title("Assignment");
		$header->import("aertrip/assignment");
		return $view;
	}

}
